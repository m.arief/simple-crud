const config = require('./config/index')
const restify = require('restify')
const mongodb = require('mongodb').MongoClient

const server = restify.createServer({
  name : config.name,
  version : config.version
})

/*server.use(restify.jsonBodyParser({mapParams:true}))
server.use(restify.acceptParser(server.acceptable))
server.use(restify.queryParser({mapParams:true}))
server.use(restify.fullResponse())*/


server.listen(config.port, ()=>{
  mongodb.connect(config.db.uri, {useUnifiedTopology:true, useNewUrlParser:true})
    .then(()=>{
      console.log(
        `${config.name} ${config.version}`, 'ready to accept connections on port',`${config.port}`,'in',`${config.env}`,'environment.'
      )
    })
    .catch((err)=>{
      console.log('An error occurred while attempting to connect to MongoDB', err)
      process.exit(1)
    })
})