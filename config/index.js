'use strict'

module.exports = {
  name : process.env.APP_NAME || 'rest_api',
  version : process.env.APP_VERSION || '0.0.1',
  env : process.env.NODE_ENV || 'development',
  port : process.env.PORT || 3000,
  db : {
    uri : ''
  }
}